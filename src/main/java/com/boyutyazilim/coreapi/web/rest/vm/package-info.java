/**
 * View Models used by Spring MVC REST controllers.
 */
package com.boyutyazilim.coreapi.web.rest.vm;
